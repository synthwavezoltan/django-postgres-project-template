# Django-Postgres project template

Use this for quickstarting any Django project.

## Getting started

Create a `.env` file with such content - of course, replace anything to your taste:

```
POSTGRES_USER=root
POSTGRES_PASSWORD=<whatever_you_want>
POSTGRES_NAME=postgres
DB_PORT=<whatever_you_want>

DJANGO_IP=0.0.0.0
DJANGO_PORT=8000

SITE_URL=localhost
PWD=.
```

Then, in the root, `docker-compose up` should be sufficient.

Feel free to delete `manage.sh` if you want.